import VolumeSVG from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Volume';
import {
  ForwardedRef,
  forwardRef,
  HTMLAttributes,
  JSXElementConstructor,
  PropsWithChildren,
  ReactElement,
  ReactNode,
} from 'react';
import { Header } from '../Header';
import '@persoonlijke-regelingen-assistent/components-css/Page/Page.scss';

interface PageSpecificProps {
  alternateTheme?: boolean;
  backgroundFC?: React.FC<{}>;
  headerLeftNode?: ReactElement<any, string | JSXElementConstructor<any>> | null | undefined;
  navigationBar?: ReactNode;
}
export interface PageProps extends HTMLAttributes<HTMLDivElement>, PageSpecificProps {}

export const Page: React.ForwardRefExoticComponent<
  PageProps & {
    children?: ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { children, headerLeftNode, navigationBar, backgroundFC, alternateTheme, ...props }: PropsWithChildren<PageProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div
        className={`${alternateTheme ? 'pra-theme pra-alternate-theme' : 'pra-theme'} pra-page`}
        ref={ref}
        {...props}
      >
        {backgroundFC && backgroundFC({ className: 'pra-page-background' })}
        <main>{children}</main>
        <footer>{navigationBar}</footer>
        <header>
          {headerLeftNode && (
            <Header leftNode={headerLeftNode} rightNode={VolumeSVG({ fill: 'var(--utrecht-icon-color)' })} />
          )}
        </header>
      </div>
    );
  },
);

Page.displayName = 'Page';
