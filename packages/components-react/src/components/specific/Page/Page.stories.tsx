import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Page/README.md?raw';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/alternate/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import type { Meta, StoryObj } from '@storybook/react';
import { CSSProperties } from 'react';
import { Page } from './index';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { Icon } from '../../generic/Icon';
import { IntroductionBackground } from '../IntroductionBackground';
import { PraNavigationBar } from '../PraNavigationBar';

const meta: Meta<typeof Page> = {
  component: Page,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/SpecificComponents/Page',
  parameters: {
    layout: 'fullscreen',
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'page',
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
  },
};

export default meta;
type Story = StoryObj<typeof Page>;

export const Default: Story = {
  args: {
    alternateTheme: false,
    backgroundFC: undefined,
    headerLeftNode: (
      <BreadcrumbNav>
        <BreadcrumbNavLink
          style={{ '--utrecht-icon-inset-block-start': '-0.1em' } as CSSProperties}
          current={false}
          href={``}
          index={0}
        >
          <Icon>{LayoutGrid({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavLink>
        <BreadcrumbNavSeparator>
          <Icon>{ChevronRight({ fill: 'var(--utrecht-icon-color)' })}</Icon>
        </BreadcrumbNavSeparator>

        <BreadcrumbNavLink current={false} href={``} index={1}>
          Thema&apos;s
        </BreadcrumbNavLink>
      </BreadcrumbNav>
    ),
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
  },
};
export const WithBackground: Story = {
  args: {
    ...Default.args,
    alternateTheme: true,
    backgroundFC: (props) => <IntroductionBackground {...props} />,
  },
};

export const WithoutNavigation: Story = {
  args: {
    ...Default.args,
    navigationBar: undefined,
  },
};

export const WithoutHeader: Story = {
  args: {
    ...Default.args,
    headerLeftNode: undefined,
  },
};
