import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import * as iconsImport from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode, SVGProps } from 'react';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Icon } from '../../generic/Icon';
import { MenuItem } from '../../generic/MenuItem';
import { Page } from '../../specific/Page';

// eslint-disable-next-line no-unused-vars
const icons = iconsImport as Record<string, (props: SVGProps<SVGSVGElement>) => JSX.Element>;
// eslint-disable-next-line no-unused-vars
const nameToIconMapper = (name: string): ((props: SVGProps<SVGSVGElement>) => JSX.Element) | undefined => {
  return icons[name];
};

interface ThemesListItem {
  icon: string;
  label: string;
  onClick: MouseEventHandler<HTMLDivElement>;
}
interface ThemesOverviewPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  themesList?: ThemesListItem[];
}
export interface ThemesOverviewPageProps extends HTMLAttributes<HTMLDivElement>, ThemesOverviewPageSpecificProps {}

export const ThemesOverviewPage: React.ForwardRefExoticComponent<
  ThemesOverviewPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { baseURL, navigationBar, themesList = [], ...props }: ThemesOverviewPageProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <Page
        headerLeftNode={
          <BreadcrumbNav>
            <BreadcrumbNavLink
              style={{ '--utrecht-icon-inset-block-start': '-0.1em' } as CSSProperties}
              current={false}
              href={`${baseURL}dashboard`}
              index={0}
            >
              <Icon>{LayoutGrid({})}</Icon>
            </BreadcrumbNavLink>
            <BreadcrumbNavSeparator>
              <Icon>{ChevronRight({})}</Icon>
            </BreadcrumbNavSeparator>

            <BreadcrumbNavLink current={false} href={`${baseURL}themes-overview`} index={1}>
              Thema&apos;s
            </BreadcrumbNavLink>
          </BreadcrumbNav>
        }
        navigationBar={navigationBar}
        {...props}
        ref={ref}
      >
        <GridLayout templateColumns={6}>
          <GridLayoutCell columnSpan={6}>
            <div
              style={
                {
                  display: 'flex',
                  'flex-direction': 'column',
                  'align-items': 'start',
                  'justify-content': 'center',
                  gap: '24px',
                } as CSSProperties
              }
            >
              {themesList.map((themesListItem: ThemesListItem) => {
                const { label, icon, onClick } = themesListItem;
                const _icon = nameToIconMapper(icon);
                return (
                  <MenuItem
                    key={label}
                    icon={_icon ? _icon({}) : undefined}
                    label={label}
                    mode="horizontal"
                    onClick={onClick}
                  />
                );
              })}
            </div>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  },
);

ThemesOverviewPage.displayName = 'ThemesOverviewPage';
