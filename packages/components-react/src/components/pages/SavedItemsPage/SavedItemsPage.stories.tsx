import muslimWeddingImg from '@persoonlijke-regelingen-assistent/assets/dist/images/muslim-wedding.png';
import pregnancyImg from '@persoonlijke-regelingen-assistent/assets/dist/images/pregnancy.png';
import studentGraduation from '@persoonlijke-regelingen-assistent/assets/dist/images/student-graduation.png';
import type { Meta, StoryObj } from '@storybook/react';
import { SavedItemsPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof SavedItemsPage> = {
  component: SavedItemsPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/SavedItemsPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof SavedItemsPage>;

export const Default: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Profiel"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-saveditemspage--docs#',
    lifeEventClickAction: (id: number) => {
      console.log(id);
    },
    regulationClickAction: (id: number) => {
      console.log(id);
    },
    profiles: [
      { name: 'Alles', pressed: true },
      { name: 'Loes', pressed: false },
      { name: 'Oma', pressed: false },
    ],
    savedRegulations: [
      { type: 'regulation', id: 0, title: 'DigiD aanvragen', message: 'Rijksoverheid' },
      { type: 'regulation', id: 1, title: 'DigiD aanvragen', message: 'Rijksoverheid' },
      { type: 'regulation', id: 2, title: 'DigiD aanvragen', message: 'Rijksoverheid' },
    ],
    savedLifeEvents: [
      { type: 'lifeEvent', id: 0, label: 'Trouwen', imageSrc: muslimWeddingImg },
      { type: 'lifeEvent', id: 1, label: 'Kind krijgen', imageSrc: pregnancyImg },
      { type: 'lifeEvent', id: 2, label: 'Studeren', imageSrc: studentGraduation },
    ],
    savedNews: [
      { type: 'news', id: 0, title: 'DigiD aanvragen', message: 'Rijksoverheid', notificationMessage: 'Nog 3 dagen' },
    ],
  },
};

export const NoSavedItems: Story = {
  args: {
    ...Default.args,
    profiles: [
      { name: 'Alles', pressed: true },
      { name: 'Loes', pressed: false },
      { name: 'Oma', pressed: false },
    ],
    savedRegulations: [],
    savedLifeEvents: [],
    savedNews: [],
  },
};
