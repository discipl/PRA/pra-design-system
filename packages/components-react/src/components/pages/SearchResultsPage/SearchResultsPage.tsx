import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import Microphone from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Microphone';
import { Gezicht } from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak';
import ErrorImg from '@persoonlijke-regelingen-assistent/assets/dist/images/error.png';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, ReactNode } from 'react';
import { Button } from '../../generic/Button';
import { ButtonBadge } from '../../generic/ButtonBadge';
import { ButtonGroup } from '../../generic/ButtonGroup';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { Image } from '../../generic/Image';
import { LabelBadge } from '../../generic/LabelBadge';
import { Link } from '../../generic/Link';
import { Paragraph } from '../../generic/Paragraph';
import { SearchResultsItem } from '../../generic/SearchResultsItem';
import { SearchResultsList } from '../../generic/SearchResultsList';
import { Textbox } from '../../generic/Textbox';
import { UnorderedList } from '../../generic/UnorderedList';
import { UnorderedListItem } from '../../generic/UnorderedListItem';
import { Page } from '../../specific/Page';

interface SearchResultsPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  numberOfResults: number;
  error?: boolean;
}
export interface SearchResultsPageProps extends HTMLAttributes<HTMLDivElement>, SearchResultsPageSpecificProps {}

export const SearchResultsPage: React.ForwardRefExoticComponent<
  SearchResultsPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ numberOfResults, error, ...props }: SearchResultsPageProps, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <Page {...props} ref={ref}>
      <GridLayout templateColumns={0}>
        <GridLayoutCell columnSpan={6}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              inlineSize: '100%',
              gap: '8px',
              alignItems: 'center',
            }}
          >
            <Textbox
              placeholder="Type hier..."
              iconStart={Microphone({
                style: { maxWidth: '100%', maxHeight: '100%' },
              })}
              invalid={false}
            />
            <Icon style={{ '--utrecht-icon-size': '80px' } as CSSProperties}>{Gezicht({})}</Icon>
          </div>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} justifyContent="end" alignItems="end">
          <ButtonGroup
            style={{
              inlineSize: '100%',
              justifyContent: 'end',
            }}
          >
            <Button
              appearance="subtle-button"
              style={
                {
                  '--utrecht-button-padding-inline-start': '0px',
                  '--utrecht-button-padding-inline-end': '0px',
                  '--utrecht-button-min-inline-size': '16px',
                } as CSSProperties
              }
            >
              +
            </Button>
            <ButtonBadge pressed={false} appearance={'primary'}>
              Ik
            </ButtonBadge>
            <ButtonBadge pressed={false} appearance={'primary'}>
              Oma
            </ButtonBadge>
          </ButtonGroup>
          <ButtonGroup
            style={{
              inlineSize: '100%',
              justifyContent: 'end',
            }}
          >
            <LabelBadge variant={'secondary'}>Particulier</LabelBadge>
            <LabelBadge variant={'secondary'}>Student</LabelBadge>
            <LabelBadge variant={'secondary'}>Thuiswonend</LabelBadge>
          </ButtonGroup>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} justifyContent="end" alignItems="end">
          <Link>
            <Icon style={{ maxHeight: '10px' }}>{ChevronDown({})}</Icon>
            {`Bekijk alle filters`}
          </Link>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} alignItems="center">
          <hr
            style={{
              width: '100%',
            }}
          />
        </GridLayoutCell>
        {error ? (
          <GridLayoutCell columnSpan={6} alignItems="center">
            <Heading level={1}>Oeps!</Heading>
            <Heading level={2}>Er is iets misgegaan...</Heading>
            <Image src={ErrorImg} className="utrecht-img--scale-down" alt={'error'}></Image>
            <Heading level={2}>Zoek op keywords of probeer een van de links hieronder:</Heading>
            <Paragraph>
              <Link href="#">Bereken je besparing met een waterpomp</Link>
            </Paragraph>
            <Paragraph>
              <Link href="#">ISDE- subsidie isolatiemaatregelen en water..</Link>
            </Paragraph>
            <UnorderedList>
              <UnorderedListItem markerContent={ChevronRight({ fill: 'var(--pra-color-primary-600)' })}>
                <Link href="#">ISDE subsidie voorwaarden</Link>
              </UnorderedListItem>
              <UnorderedListItem markerContent={ChevronRight({ fill: 'var(--pra-color-primary-600)' })}>
                <Link href="#">ISDE subsidie bedragen</Link>
              </UnorderedListItem>
            </UnorderedList>
          </GridLayoutCell>
        ) : null}
        {!error ? (
          <GridLayoutCell columnSpan={6} alignItems="end">
            <Paragraph small style={{ color: 'var(--pra-color-primary-400)' }}>
              {`${numberOfResults} resultaten`}
            </Paragraph>
          </GridLayoutCell>
        ) : null}
        {!error &&
          (numberOfResults > 0 ? (
            <GridLayoutCell columnSpan={6} paddingInline={0}>
              <SearchResultsList>
                {[...Array(numberOfResults)].map((e, i) => (
                  <SearchResultsItem
                    title="Titel Regeling"
                    subTitle="Overheidsinstantie"
                    description="01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt. Purus tellus at nunc amet quis ut augue tincidunt. Purus tellus at nunc amet quis ut augue tincidunt."
                    key={i}
                  />
                ))}
              </SearchResultsList>
            </GridLayoutCell>
          ) : (
            <GridLayoutCell columnSpan={6}>
              <Heading level={1}>Helaas er zijn geen regelingen gevonden</Heading>
              <Paragraph>
                Probeer het opnieuw. Je kan zoeken op de volgende manieren:
                <ul>
                  <li>Woorden in de naam/titel van de regeling</li>
                  <li>Woorden in de omschrijving van de regeling</li>
                  <li>De naam van de organisatie die verantwoordelijk is voor de regeling</li>
                </ul>
              </Paragraph>
            </GridLayoutCell>
          ))}
      </GridLayout>
    </Page>
  );
});

SearchResultsPage.displayName = 'SearchResultsPage';
