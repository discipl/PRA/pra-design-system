import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import SVGGezicht from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Gezicht';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { CSSProperties, ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode, useState } from 'react';
import { BreadcrumbNav } from '../../generic/BreadcrumbNav';
import { BreadcrumbNavLink } from '../../generic/BreadcrumbNavLink';
import { BreadcrumbNavSeparator } from '../../generic/BreadcrumbNavSeparator';
import { Dropdown } from '../../generic/Dropdown';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Icon } from '../../generic/Icon';
import { Indicator } from '../../generic/Indicator';
import { NotificationMessage } from '../../generic/NotificationMessage/NotificationMessage';
import { SpeachBulb } from '../../generic/SpeachBulb';
import { Page } from '../../specific/Page';

interface RegulationContent {
  ContentElements: (string | JSX.Element | JSX.Element[] | null)[];
  ContentType: 'Plain' | 'Expandable' | 'ExpandableSteps';
  ContentId: number;
  Label?: string;
}
interface RegulationPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
  indicator?: {
    showIndicator?: boolean;
    bookmarkButtonAction?: MouseEventHandler;
    isBookmarked: boolean;
  };
  speachBulb?: {
    body?: string;
    hyperlinkLabel?: string;
    hyperlinkClickHandler?: MouseEventHandler;
    title?: string;
  };
  regulation: {
    id?: number;
    title?: string;
    minutesIndication?: number;
    authority?: string;
    imgSrc?: string;
    description?: string | JSX.Element | JSX.Element[] | null;
    content?: RegulationContent[];
  };
}
export interface RegulationPageProps extends HTMLAttributes<HTMLDivElement>, RegulationPageSpecificProps {}

export const RegulationPage: React.ForwardRefExoticComponent<
  RegulationPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { navigationBar, baseURL, indicator, speachBulb, regulation = {}, ...props }: RegulationPageProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const { showIndicator = false, bookmarkButtonAction = () => {}, isBookmarked = false } = indicator || {};
    const {
      body: speachBulbBody = '',
      hyperlinkLabel: speachBulbHyperLinkLabel = '',
      hyperlinkClickHandler: speachBulbHyperlinkClickHandler = () => {},
      title: speachBulbTitle = '',
    } = speachBulb || {};
    const {
      title = '',
      id = 0,
      minutesIndication = 0,
      authority = '',
      description = '',
      content = [],
      imgSrc,
    } = regulation;
    const [showSpeachBulb, setShowSpeachBulb] = useState<boolean>(true);
    return (
      <Page
        navigationBar={navigationBar}
        {...props}
        ref={ref}
        headerLeftNode={
          <BreadcrumbNav>
            <BreadcrumbNavLink
              style={
                {
                  '--utrecht-icon-inset-block-start': '-0.1em',
                } as CSSProperties
              }
              current={false}
              href={`${baseURL}dashboard`}
              index={0}
            >
              <Icon>{LayoutGrid({})}</Icon>
            </BreadcrumbNavLink>
            <BreadcrumbNavSeparator>
              <Icon>{ChevronRight({})}</Icon>
            </BreadcrumbNavSeparator>
            <BreadcrumbNavLink current={false} href={`${baseURL}search-results`} index={1}>
              Zoeken
            </BreadcrumbNavLink>
            <BreadcrumbNavSeparator>
              <Icon>{ChevronRight({})}</Icon>
            </BreadcrumbNavSeparator>
            <BreadcrumbNavLink current={true} href={`${baseURL}regulations/${id}`} index={2}>
              {title}
            </BreadcrumbNavLink>
          </BreadcrumbNav>
        }
      >
        <GridLayout templateColumns={12}>
          <GridLayoutCell columnSpan={11} alignItems="start">
            <Heading level={4} style={{ color: 'var(--pra-color-secondary-600' }}>
              {authority}
            </Heading>
            <Heading level={1} style={{ margin: 0 }}>
              {title}
            </Heading>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={1} alignItems="end" justifyContent="start" style={{ paddingInlineStart: '0' }}>
            <Icon
              style={
                {
                  '--utrecht-icon-size': '55px',
                  backgroundColor: 'white',
                  borderRadius: '50%',
                  padding: '2px',
                  anchorName: '--anchor-el',
                } as CSSProperties
              }
            >
              {SVGGezicht({})}
            </Icon>
          </GridLayoutCell>
          {speachBulbBody.length > 0 && (
            <GridLayoutCell columnSpan={12}>
              <SpeachBulb shown={showSpeachBulb}>
                <NotificationMessage
                  notificationTitle={speachBulbTitle}
                  message={speachBulbBody}
                  buttonLabel={speachBulbHyperLinkLabel}
                  closeHandler={() => {
                    setShowSpeachBulb(!showSpeachBulb);
                  }}
                  buttonHandler={(event) => speachBulbHyperlinkClickHandler(event)}
                ></NotificationMessage>
              </SpeachBulb>
            </GridLayoutCell>
          )}
          {showIndicator && (
            <GridLayoutCell columnSpan={12}>
              <Indicator
                indicationInMins={minutesIndication}
                bookmarkOnClick={(event) => bookmarkButtonAction(event)}
                isBookmarked={isBookmarked}
              />
            </GridLayoutCell>
          )}
          <GridLayoutCell columnSpan={12}>{imgSrc && <img style={{ maxWidth: '100%' }} src={imgSrc} />}</GridLayoutCell>
          {content.length > 0
            ? content.map(({ ContentElements, ContentType = 'Plain', ContentId, Label }) => {
                if (ContentType === 'Plain') {
                  return (
                    <GridLayoutCell key={ContentId} columnSpan={12}>
                      {ContentElements}
                    </GridLayoutCell>
                  );
                } else if (ContentType === 'Expandable') {
                  return (
                    <GridLayoutCell key={ContentId} columnSpan={12}>
                      <Dropdown key={ContentId} title={Label} variant="Default">
                        {ContentElements}
                      </Dropdown>
                    </GridLayoutCell>
                  );
                } else if (ContentType === 'ExpandableSteps') {
                  return (
                    <GridLayoutCell key={ContentId} columnSpan={12}>
                      <Dropdown key={ContentId} title={Label} variant="Stappenplan">
                        {ContentElements}
                      </Dropdown>
                    </GridLayoutCell>
                  );
                }
                return null;
              })
            : description}
        </GridLayout>
      </Page>
    );
  },
);

RegulationPage.displayName = 'RegulationPage';
