import AlertTriangle from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/AlertTriangle';
import retirement_check from '@persoonlijke-regelingen-assistent/assets/dist/images/retirement-check.png';
import type { Meta, StoryObj } from '@storybook/react';
import { RegulationPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { Button } from '../../generic/Button';
import { Heading } from '../../generic/Heading';
import { InformationAlert } from '../../generic/InformationAlert';
import { Link } from '../../generic/Link';
import { Paragraph } from '../../generic/Paragraph';
import { DonutChartWithDetails } from '../../specific/DonutChartWithDetails';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof RegulationPage> = {
  component: RegulationPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/RegulationPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof RegulationPage>;

export const Default: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-regulationpage--docs#',
    indicator: {
      bookmarkButtonAction: () => {},
    },
    regulation: {
      id: 12569,
      title: 'Individuele studietoeslag aanvragen',
      minutesIndication: 5,
      authority: 'Aalsmeer',
      description: (
        <p className="utrecht-paragraph">
          Veel studenten hebben een bijbaan om hun studie te kunnen betalen. Als student met een beperking kunt u dit
          niet altijd. Misschien komt u in aanmerking voor een individuele studietoeslag.
        </p>
      ),
      content: [
        {
          ContentElements: [
            <h4 className="utrecht-heading-4">Wat is de Participatiewet?</h4>,
            <p className="utrecht-paragraph">
              Iedereen die kan werken maar het op de arbeidsmarkt zonder ondersteuning niet redt, valt onder de
              Participatiewet. De wet moet ervoor zorgen dat meer mensen werk vinden, ook mensen met een
              arbeidsbeperking.
            </p>,
          ],
          ContentType: 'Plain',
          ContentId: 1,
        },
        {
          ContentElements: [
            <h5 className="utrecht-heading-5">Wanneer heb ik recht op algemene bijstand?</h5>,
            <p className="utrecht-paragraph">
              U heeft recht op een algemene bijstandsuitkering als u voldoet aan de voorwaarden. En u niet genoeg
              inkomen of vermogen heeft om van te leven. En u ook niet in aanmerking komt voor een andere vorm van hulp
              of uitkering.
            </p>,
            <h6 className="utrecht-heading-6">Voorwaarden algemene bijstand</h6>,
            <p className="utrecht-paragraph">
              U heeft in Nederland recht op algemene&nbsp;bijstand als u voldoet aan de volgende voorwaarden:
            </p>,
            <ul className="pra-unordered-list">
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                u woont in Nederland. En u bent Nederlander of u heeft een geldige verblijfsvergunning;
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                u bent 18 jaar of ouder;
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                u heeft niet genoeg inkomen of vermogen om van te leven. Bijvoorbeeld om een woning te huren en eten te
                kopen;
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                u komt niet in aanmerking voor een andere vorm van hulp of uitkering;
              </li>
              <li className="pra-unordered-list-item">
                <span className="pra-unordered-list-item-marker">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <path
                      fill="#223C61"
                      fill-rule="evenodd"
                      d="M8.293 5.293a1 1 0 0 1 1.414 0l6 6a1 1 0 0 1 0 1.414l-6 6a1 1 0 0 1-1.414-1.414L13.586 12 8.293 6.707a1 1 0 0 1 0-1.414"
                      clip-rule="evenodd"
                    ></path>
                  </svg>
                </span>
                u zit niet in de gevangenis of een huis van bewaring.
              </li>
            </ul>,
            <p className="utrecht-paragraph">
              Er geldt ook een&nbsp;aantal{' '}
              <a href="https://www.rijksoverheid.nl/onderwerpen/participatiewet/vraag-en-antwoord/wat-zijn-mijn-rechten-en-plichten-in-de-bijstand">
                verplichtingen als u een bijstandsuitkering ontvangt
              </a>
              .
            </p>,
          ],
          ContentType: 'Expandable',
          ContentId: 2,
          Label: 'Algemene bijstand',
        },
      ],
    },
  },
};

export const RetirementCheck: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-regulationpage--retirement-check#',
    indicator: {
      bookmarkButtonAction: () => {},
    },
    speachBulb: {
      body: 'Wilt u meer weten over uw persoonlijke situatie? Dan kunnen wij een indicatieve berekening van pensioenuitkeringen en te betalen belasting voor u maken. Daarmee wordt duidelijke of u wellicht iets moet terugbetalen.',
      hyperlinkLabel: 'Doe de check',
      hyperlinkClickHandler: () => {},
      title: 'Pensioen checken',
    },
    regulation: {
      id: 1,
      title: 'Pensioen: Wat moet ik checken?',
      authority: 'Belastingdienst',
      imgSrc: retirement_check,
      description: (
        <p className="utrecht-paragraph">
          Op basis van de gegevens van de Belastingdienst is gebleken dat het soms voorkomt dat er over uw
          pensioenuitkeringen te weinig belasting wordt ingehouden. Mogelijk is dit het geval voor u en moet u de
          verschuldigde belasting bij de definitieve aanslag in één keer betalen. <a href="">Meer informatie</a> over
          deze situatie.
        </p>
      ),
      content: [
        {
          ContentElements: [
            <p className="utrecht-paragraph">
              Op basis van de gegevens van de Belastingdienst is gebleken dat het soms voorkomt dat er over uw
              pensioenuitkeringen te weinig belasting wordt ingehouden. Mogelijk is dit het geval voor u en moet u de
              verschuldigde belasting bij de definitieve aanslag in één keer betalen. <a href="#">Meer informatie</a>{' '}
              over deze situatie.
            </p>,
          ],
          ContentType: 'Plain',
          ContentId: 0,
        },
        {
          ContentElements: [
            <p className="utrecht-paragraph">
              We streven ernaar alles zo goed mogelijk voor u te regelen. U kunt zelf het volgende doen:
              <ul>
                <li>Dien een voorlopige aangifte inkomstenbelasting in.</li>
                <li>Wijzig een lopende voorlopige aanslag inkomstenbelasting.</li>
              </ul>
              <br />
              We vertellen u hierna hoe u dit kunt doen. In de volgende stappen vragen we u om een aantal gegevens, uw
              Pensioen-overzicht. Daarmee maken we een handig overzicht van de berekening.
              <br />
              <br />
              <a href="">Meer informatie</a> over het verschil tussen een voorlopige aangifte en lopende voorlopige
              aanslag inkomsten belasting.
            </p>,
          ],
          ContentType: 'Expandable',
          ContentId: 1,
          Label: 'Wat kunt u doen?',
        },
        {
          ContentElements: [
            <h3>Heeft u vragen?</h3>,
            <p className="utrecht-paragraph">
              Bel dan met de BelastingTelefoon via <a href="">0800-0543</a> (gratis).
            </p>,
          ],
          ContentType: 'Plain',
          ContentId: 2,
          Label: 'Wat kunt u doen?',
        },
        {
          ContentElements: [
            <Button
              appearance="primary"
              style={{
                backgroundColor: 'var(--pra-color-primary-600)',
                color: 'var(--voorbeeld-color-white)',
                minInlineSize: '100%',
              }}
            >
              Doe de check
            </Button>,
          ],
          ContentType: 'Plain',
          ContentId: 3,
        },
      ],
    },
  },
};

const NogTeBetalenInkomstenbelasting = 8264;
const LoonheffingTotaal = 14924;
const Inkomstenbelasting = 23188;
const AowBedrag = 18031;
const Fonds1 = 10795;
const Fonds2 = 50000;
const Fonds1Label = 'Pensioenfonds 1';
const Fonds2Label = 'Pensioenfonds 2';
const BrutoPensioenTotaal = 78826;
const PotjesTotaal = 60795;

export const RetirementCheckCalculation: Story = {
  args: {
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Dashboard"
        indicators={[]}
      />
    ),
    baseURL: '/?path=/docs/pra-ds-pages-regulationpage--retirement-check-calculation#',
    indicator: {
      bookmarkButtonAction: () => {},
    },
    regulation: {
      id: 2,
      title: 'Berekening overzicht',
      authority: 'Belastingdienst',
      content: [
        {
          ContentElements: [
            <InformationAlert icon={AlertTriangle} title="Let op">
              <p style={{ margin: 0 }}>
                Het getoonde bedrag is een schatting en afhankelijk van diverse <a href="#">factoren en keuzes</a>.
              </p>
            </InformationAlert>,
          ],
          ContentType: 'Plain',
          ContentId: 1,
        },
        {
          ContentElements: [
            <DonutChartWithDetails
              HeaderAmount={{
                Label: 'Schatting nog te betalen bedrag door u',
                Color: 'var(--pra-color-secondary-300)',
                AmountType: 'Single',
                Amount: NogTeBetalenInkomstenbelasting, //"aow-belasting/inkomstenbelasting_aow/NogTeBetalenInkomstenbelasting"
              }}
              Calculations={[
                {
                  Label: 'Inkomen',
                  CalculationType: 'Summation',
                  Amounts: [
                    {
                      Label: 'Pensioenuitkeringen',
                      Color: 'var(--pra-color-secondary-200)',
                      AmountType: 'Total',
                      Parts: {
                        Values: [Fonds1, Fonds2],
                        Labels: [Fonds1Label, Fonds2Label],
                      },
                      Amount: PotjesTotaal, //"aow-belasting/inkomstenbelasting_aow/PotjesTotaal",
                    },
                    {
                      Label: 'AOW',
                      Color: 'var(--pra-color-secondary-600)',
                      AmountType: 'Single',
                      Amount: AowBedrag, //'aow-belasting/inkomstenbelasting_aow/AowBedrag',
                    },
                    {
                      Label: 'Overig inkomen',
                      AmountType: 'Unknown',
                    },
                  ],
                  TotalAmount: {
                    Label: 'Totaal bruto-inkomen op jaarbasis',
                    AmountType: 'Single',
                    Amount: BrutoPensioenTotaal, //'aow-belasting/inkomstenbelasting_aow/BrutoPensioenTotaal',
                  },
                },
                {
                  Label: 'Inkomstenbelasting',
                  CalculationType: 'Substraction',
                  TotalAmount: {
                    Label: 'Totaal verschuldigde inkomstenbelasting',
                    AmountType: 'Single',
                    Amount: Inkomstenbelasting, //'aow-belasting/inkomstenbelasting_aow/Inkomstenbelasting',
                  },
                  Amounts: [
                    {
                      Label: 'Loonheffing',
                      Color: 'var(--pra-color-secondary-400)',
                      AmountType: 'Single',
                      Amount: LoonheffingTotaal, //'aow-belasting/inkomstenbelasting_aow/LoonheffingTotaal',
                    },
                  ],
                  ResultingAmount: {
                    Label: 'Schatting nog te betalen bedrag',
                    Color: 'var(--pra-color-secondary-300)',
                    AmountType: 'Single',
                    Amount: NogTeBetalenInkomstenbelasting, //"aow-belasting/inkomstenbelasting_aow/NogTeBetalenInkomstenbelasting"
                  },
                },
              ]}
            />,
          ],
          ContentType: 'Plain',
          ContentId: 2,
        },
        {
          ContentElements: [<Heading level={3}>De berekening</Heading>],
          ContentType: 'Plain',
          ContentId: 4,
        },
        {
          ContentElements: [
            <Paragraph>
              Dit bedrag is een schatting en kan afwijken van het werkelijke bedrag dat u aan de belastingdienst moet
              betalen. De hoogte van het bedrag hangt af van verschillende factoren en keuzes. Denk aan, andere bronnen
              van inkomsten. Voor meer duidelijkheid over het definitieve bedrag kunt u de voorlopige aangifte
              inkomstenbelasting indienen. <Link>Meer uitleg</Link> over de berekening nodig?
            </Paragraph>,
          ],
          ContentType: 'Plain',
          ContentId: 5,
        },
      ],
    },
  },
};
