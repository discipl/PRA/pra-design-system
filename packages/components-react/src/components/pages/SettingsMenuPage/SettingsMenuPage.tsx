import React, { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler, ReactNode } from 'react';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Page } from '../../specific/Page';
import '@persoonlijke-regelingen-assistent/components-css/SettingsMenuPage/SettingsMenuPage.scss';

interface SettingsMenuPageSpecificProps {
  menuItems: { label: string; action: MouseEventHandler }[];
  menuTitle: string;
  navigationBar?: ReactNode;
}
export interface SettingsMenuPageProps extends HTMLAttributes<HTMLDivElement>, SettingsMenuPageSpecificProps {}

export const SettingsMenuPage: React.ForwardRefExoticComponent<
  SettingsMenuPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ menuTitle, menuItems, navigationBar, ...props }: SettingsMenuPageProps, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <Page alternateTheme={false} ref={ref} navigationBar={navigationBar} {...props}>
        <GridLayout templateColumns={6}>
          <GridLayoutCell columnSpan={6}>
            <Heading level={1}>{menuTitle}</Heading>
            <div className="pra-settings-menu-list">
              {menuItems.map((item) => {
                const { label, action } = item;
                return (
                  <Heading level={2} className="pra-settings-menu-list-item" onClick={action}>
                    {label}
                  </Heading>
                );
              })}
            </div>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  },
);

SettingsMenuPage.displayName = 'SettingsMenuPage';
