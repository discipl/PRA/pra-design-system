import type { Meta, StoryObj } from '@storybook/react';
import { SettingsMenuPage } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

const meta: Meta<typeof SettingsMenuPage> = {
  component: SettingsMenuPage,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Pages/SettingsMenuPage',
  parameters: {
    ['--pra-component-design-version-name']: '0.0.0',
    ['--pra-render-in-mobile-viewport']: true,
    layout: 'fullscreen',
  },
};

export default meta;
type Story = StoryObj<typeof SettingsMenuPage>;

export const Default: Story = {
  args: {
    menuItems: [
      { label: 'Persoonlijke gegevens', action: () => {} },
      { label: 'Meldingen', action: () => {} },
      { label: 'Toegankelijkheid', action: () => {} },
      { label: 'Over de PRA', action: () => {} },
      { label: 'Beveiliging en privacy', action: () => {} },
      { label: 'Tips en support', action: () => {} },
    ],
    menuTitle: 'Instellingen',
    navigationBar: (
      <PraNavigationBar
        dashboardOnClickHandler={() => {}}
        settingsOnClickHandler={() => {}}
        profileOnClickHandler={() => {}}
        selectedItem="Instellingen"
        indicators={[]}
      />
    ),
  },
};
