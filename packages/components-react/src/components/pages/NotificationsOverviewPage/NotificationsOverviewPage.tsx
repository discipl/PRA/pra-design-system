import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { ForwardedRef, forwardRef, HTMLAttributes } from 'react';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { NotificationItem } from '../../generic/NotificationItem';
import { Paragraph } from '../../generic/Paragraph';
import { Tabs } from '../../generic/Tabs';
import { Textbox } from '../../generic/Textbox';
import { Page } from '../../specific/Page';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

interface NotificationsOverviewPageSpecificProps {}
export interface NotificationsOverviewPageProps
  extends HTMLAttributes<HTMLDivElement>,
    NotificationsOverviewPageSpecificProps {}

export const NotificationsOverviewPage: React.ForwardRefExoticComponent<
  NotificationsOverviewPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ ...props }: NotificationsOverviewPageProps, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <Page
      navigationBar={
        <PraNavigationBar
          dashboardOnClickHandler={() => {}}
          settingsOnClickHandler={() => {}}
          profileOnClickHandler={() => {}}
          selectedItem="Profiel"
          indicators={[]}
        />
      }
      {...props}
      ref={ref}
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <Textbox placeholder="Zoekterm" iconStart={Search({})} invalid={false} />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} alignItems="center">
          <Tabs
            tabData={[
              {
                label: 'Meldingen',
                panelContent: (
                  <GridLayout templateColumns={6}>
                    <GridLayoutCell columnSpan={6} paddingInline={0}>
                      <div style={{ display: 'flex', flexDirection: 'column', gap: '16px' }}>
                        <Heading level={3}>Vandaag</Heading>
                        <NotificationItem
                          newItem={true}
                          title="Keuze orgaandonatie"
                          timeIndication="3u"
                          message="Sinds augustus 2021 is iedereen automatisch orgaandonor. Wil je dit of jouw voorkeuren aanpassen?Dat kan. Ga naar www.donorregister.nl"
                        />
                      </div>
                    </GridLayoutCell>
                    <GridLayoutCell columnSpan={6} paddingInline={0}>
                      <div style={{ display: 'flex', flexDirection: 'column', gap: '16px' }}>
                        <Heading level={3}>Gisteren</Heading>
                        <NotificationItem
                          title="Heb je al een zorgverzekering?"
                          timeIndication="22u"
                          message="Nog van harte gefeliciteerd namens jouw Persoonlijke Regelingen Assistent! Er zijn een aantal dingen die je moet regelen als je 18 jaar"
                        />
                        <NotificationItem
                          title="Je bent onlangs 18 jaar geworden"
                          timeIndication="16u"
                          message="Nog van harte gefeliciteerd namens jouw Persoonlijke Regelingen Assistent! Er zijn een aantal dingen die je moet regelen als je 18 jaar"
                        />
                      </div>
                    </GridLayoutCell>
                  </GridLayout>
                ),
              },
              {
                label: 'De assistent',
                panelContent: <Paragraph>De openstaande taken</Paragraph>,
              },
            ]}
          />
        </GridLayoutCell>
      </GridLayout>
    </Page>
  );
});

NotificationsOverviewPage.displayName = 'NotificationsOverviewPage';
