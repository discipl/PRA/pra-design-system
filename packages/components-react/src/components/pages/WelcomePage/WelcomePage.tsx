import man_with_laptop from '@persoonlijke-regelingen-assistent/assets/dist/images/man-with-laptop.png';
import { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler } from 'react';
import { Button } from '../../generic/Button';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Image } from '../../generic/Image';
import { Page } from '../../specific/Page';

interface WelcomePageSpecificProps {
  startButtonAction: MouseEventHandler;
}
export interface WelcomePageProps extends HTMLAttributes<HTMLDivElement>, WelcomePageSpecificProps {}

export const WelcomePage: React.ForwardRefExoticComponent<WelcomePageProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(({ startButtonAction, ...props }: WelcomePageProps, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <Page ref={ref} {...props}>
        <GridLayout templateColumns={6} style={{ gridTemplateRows: 'min-content min-content auto' }}>
          <GridLayoutCell columnSpan={6}>
            <Heading level={1}>Welkom Loes, bij jouw Persoonlijke Regelingen Assistent!</Heading>
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6} paddingInline={0}>
            <Image src={man_with_laptop} className="utrecht-img--scale-down" alt="Man met een laptop" />
          </GridLayoutCell>
          <GridLayoutCell columnSpan={6} style={{ height: 'auto' }} justifyContent="end">
            <Button
              style={{ inlineSize: '100%', maxInlineSize: '100%' }}
              appearance="primary-action-button"
              onClick={startButtonAction}
            >
              {`Start de PRA`}
            </Button>
          </GridLayoutCell>
        </GridLayout>
      </Page>
    );
  });

WelcomePage.displayName = 'WelcomePage';
