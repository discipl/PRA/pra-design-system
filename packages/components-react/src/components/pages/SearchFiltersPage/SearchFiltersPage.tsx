import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { ForwardedRef, forwardRef, HTMLAttributes, ReactNode } from 'react';
import { Button } from '../../generic/Button';
import { ButtonBadge } from '../../generic/ButtonBadge';
import { ButtonGroup } from '../../generic/ButtonGroup';
import { Checkbox } from '../../generic/Checkbox';
import { FormField } from '../../generic/FormField';
import { GridLayout } from '../../generic/GridLayout';
import { GridLayoutCell } from '../../generic/GridLayoutCell';
import { Heading } from '../../generic/Heading';
import { Textbox } from '../../generic/Textbox';
import { Page } from '../../specific/Page';
import { PraNavigationBar } from '../../specific/PraNavigationBar';

interface SearchFiltersPageSpecificProps {
  navigationBar?: ReactNode;
  baseURL?: string;
}
export interface SearchFiltersPageProps extends HTMLAttributes<HTMLDivElement>, SearchFiltersPageSpecificProps {}

export const SearchFiltersPage: React.ForwardRefExoticComponent<
  SearchFiltersPageProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ ...props }: SearchFiltersPageProps, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <Page
      navigationBar={
        <PraNavigationBar
          dashboardOnClickHandler={() => {}}
          settingsOnClickHandler={() => {}}
          profileOnClickHandler={() => {}}
          selectedItem="Dashboard"
          indicators={[]}
        />
      }
      {...props}
      ref={ref}
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <Heading level={3}>Filterprofiel toevoegen</Heading>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Naam filterprofiel</Heading>
          <Textbox placeholder="Type hier..." invalid={undefined} />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Ik zoek als:</Heading>
          <ButtonGroup>
            <ButtonBadge appearance={'primary'}>Particulier</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Organisatie</ButtonBadge>
          </ButtonGroup>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Woonsituatie:</Heading>
          <ButtonGroup>
            <ButtonBadge appearance={'primary'}>Thuiswonend</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Koophuis</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Huurwoning</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Studentenwoning</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Iets anders</ButtonBadge>
          </ButtonGroup>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Werksituatie:</Heading>
          <ButtonGroup>
            <ButtonBadge appearance={'primary'}>Student</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Werkzoekende</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Full-time baan</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Part-time baan</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Vrijwilligerswerk</ButtonBadge>
            <ButtonBadge appearance={'primary'}>Werkloos</ButtonBadge>
          </ButtonGroup>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoeken op woord of zinsdeel:</Heading>
          <Textbox placeholder="Type hier..." iconStart={Search({})} invalid={undefined} />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoekenresultaten voor:</Heading>
          <FormField label="Gemeente:" type="textbox">
            <Textbox placeholder="Zoek op gemeente" iconEnd={ChevronDown({})} invalid={undefined} />
          </FormField>
          <FormField label="Provincie:" type="textbox">
            <Textbox placeholder="Zoek op provincie" iconEnd={ChevronDown({})} invalid={undefined} />
          </FormField>
          <FormField label="Waterschappen:" type="textbox">
            <Textbox placeholder="Zoek op waterschappen" iconEnd={ChevronDown({})} invalid={undefined} />
          </FormField>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoekfilters toevoegen:</Heading>
          <Textbox placeholder="Type hier..." iconStart={Search({})} invalid={undefined} />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <FormField label="Filters instellen als zoekfilter profiel" type="checkbox" indicatorInfo=".">
            <Checkbox className="utrecht-checkbox utrecht-checkbox--html-input" required={true} />
          </FormField>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <ButtonGroup style={{ width: '100%' }}>
            <Button style={{ width: '100%' }} appearance="primary-action-button">
              Zoekfilterprofiel opslaan
            </Button>
            <Button style={{ width: '100%' }} appearance="secondary-action-button">
              Annuleren
            </Button>
          </ButtonGroup>
        </GridLayoutCell>
      </GridLayout>
    </Page>
  );
});

SearchFiltersPage.displayName = 'SearchFiltersPage';
