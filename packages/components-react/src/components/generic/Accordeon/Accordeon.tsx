import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import ChevronUp from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronUp';
import { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, useState } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Accordeon/Accordeon.scss';

interface AccordeonSpecificProps {
  title?: string;
  initCollapsed?: boolean;
}

export interface AccordeonProps extends PropsWithChildren<AccordeonSpecificProps>, HTMLAttributes<HTMLInputElement> {}

export const Accordeon: React.ForwardRefExoticComponent<AccordeonProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(({ title = '', initCollapsed = true, children }: AccordeonProps, ref: ForwardedRef<HTMLDivElement>) => {
    const [collapsed, setCollapsed] = useState<boolean>(initCollapsed);
    return (
      <div className="pra-accordeon" ref={ref}>
        <div
          className="pra-accordeon-header"
          onClick={() => {
            setCollapsed(!collapsed);
          }}
        >
          <h3 className="pra-accordeon-title">{title}</h3>
          {!collapsed && ChevronUp({})}
          {!!collapsed && ChevronDown({})}
        </div>
        {!collapsed && <div className="pra-accordeon-content">{children}</div>}
      </div>
    );
  });

Accordeon.displayName = 'Accordeon';
