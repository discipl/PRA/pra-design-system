import PdfIcon from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/Pdf';
import React, { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { Paragraph } from '../Paragraph';
import '@persoonlijke-regelingen-assistent/components-css/Upload/Upload.scss';

interface UploadSpecificProps {
  label?: string;
}

export interface UploadProps extends UploadSpecificProps {}

export const Upload: React.ForwardRefExoticComponent<
  UploadProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ label }: PropsWithChildren<UploadProps>, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <div ref={ref} className="pra-upload-container">
      <Paragraph>{label}</Paragraph>
      <Paragraph>
        <a href="#">{PdfIcon({})} Upload een bestand van uw computer</a>
      </Paragraph>
    </div>
  );
});

Upload.displayName = 'Upload';
