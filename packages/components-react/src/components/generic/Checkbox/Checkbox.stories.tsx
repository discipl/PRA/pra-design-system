import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Checkbox/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Checkbox } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Checkbox> = {
  component: Checkbox,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Checkbox',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'checkbox',
    ['--pra-component-design-version-name']: 'autosave - 4661964929',
    ['--pra-component-design-version-id']: '4661964929',
  },
};

export default meta;
type Story = StoryObj<typeof Checkbox>;

export const Default: Story = {
  args: {},
};

export const Checked: Story = {
  args: {
    ...Default.args,
    checked: true,
  },
};
export const Disabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
  },
};
export const CheckedAndDisabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
    checked: true,
  },
};
export const RequiredAndInvalid: Story = {
  args: {
    ...Default.args,
    required: true,
  },
};
