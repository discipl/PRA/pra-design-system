import Politiek from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak/Politiek';
import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Carousel/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Carousel } from './index';
import { Card } from '../Card';
import { MenuItem } from '../MenuItem';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Carousel> = {
  component: Carousel,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/Carousel',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'carousel',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

const cardComponent = (
  <Card
    imgAlt="Birthday Party"
    imgSrc={birthday_party}
    linkHref="#"
    linkLabel="Bekijk levensgebeurtenis"
    description="Je bent vandaag of onlangs 18 jaar geworden. Er zijn een aantal belangrijke zaken die je moet regelen."
    title="18 jaar worden: Wat moet ik regelen?"
  />
);
const menuItemComponent = <MenuItem icon={Politiek({})} label="Zorg" />;
export default meta;
type Story = StoryObj<typeof Carousel>;

export const Default: Story = {
  args: {
    showNavigation: false,
    slideWidth: '250px',
    slideGap: '20px',
    children: [Array(5).fill(cardComponent)],
  },
};

export const WithNavigationDots: Story = {
  args: {
    ...Default.args,
    slideGap: '100px',
    showNavigation: true,
  },
};

export const CarouselOfMenuItems: Story = {
  args: {
    ...Default.args,
    slideGap: '8px',
    slideWidth: '96px',
    children: [Array(50).fill(menuItemComponent)],
  },
};
