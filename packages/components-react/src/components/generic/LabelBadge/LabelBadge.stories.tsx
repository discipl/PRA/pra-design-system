import X from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/X';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/LabelBadge/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { LabelBadge } from './index';
import { Icon } from '../Icon';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof LabelBadge> = {
  component: LabelBadge,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/LabelBadge',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'labelbadge',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof LabelBadge>;

export const Primary: Story = {
  args: {
    variant: 'primary',
    children: ['Label', <Icon>{X({})}</Icon>],
  },
};

export const Secondary: Story = {
  args: {
    ...Primary.args,
    variant: 'secondary',
  },
};
