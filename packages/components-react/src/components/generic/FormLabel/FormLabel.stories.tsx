import ReadMe from '@persoonlijke-regelingen-assistent/components-css/FormLabel/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { FormLabel } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof FormLabel> = {
  component: FormLabel,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/FormLabel',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'formLabel',
    ['--pra-component-design-version-name']: 'autosave - 4661964929',
    ['--pra-component-design-version-id']: '4661964929',
  },
};

export default meta;
type Story = StoryObj<typeof FormLabel>;

export const Default: Story = {
  args: {
    children: ['FormLabelText'],
  },
};
