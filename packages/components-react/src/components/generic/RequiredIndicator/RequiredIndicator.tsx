import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

import '@persoonlijke-regelingen-assistent/components-css/RequiredIndicator/RequiredIndicator.scss';

interface RequiredIndicatorSpecificProps {
  required: boolean;
}
export interface RequiredIndicatorProps extends RequiredIndicatorSpecificProps {}

export const RequiredIndicator: React.ForwardRefExoticComponent<
  RequiredIndicatorProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(({ required = false }: PropsWithChildren<RequiredIndicatorProps>, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <span ref={ref} aria-hidden="true" className={`${required ? 'pra-required-indicator-required' : ''}`}>
      {required ? ' * ' : ''}
    </span>
  );
});

RequiredIndicator.displayName = 'RequiredIndicator';
