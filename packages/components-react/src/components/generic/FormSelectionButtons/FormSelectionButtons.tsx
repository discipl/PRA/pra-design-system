import { ForwardedRef, forwardRef, HTMLAttributes, useEffect, useState } from 'react';
import { ButtonBadge } from '../ButtonBadge';
import { ButtonGroup } from '../ButtonGroup';
interface FormSelectionOptions {
  value: string;
  label: string;
}
interface FormSelectionButtonsSpecificProps {
  options: FormSelectionOptions[];
  initialValue?: string;
  // eslint-disable-next-line no-unused-vars
  onValueChange?: (value: string | undefined) => void;
}
export interface FormSelectionButtonsProps extends HTMLAttributes<HTMLDivElement>, FormSelectionButtonsSpecificProps {}

export const FormSelectionButtons: React.ForwardRefExoticComponent<
  FormSelectionButtonsProps & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { options, initialValue, onValueChange, ...props }: FormSelectionButtonsProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    const [value, setValue] = useState<string | undefined>(initialValue);

    const _buttonClicked = (event: React.MouseEvent) => {
      const clickedValue = event?.currentTarget?.getAttribute('data-value') || undefined;
      if (value === clickedValue) {
        setValue(undefined);
      } else {
        setValue(clickedValue);
      }
    };

    if (onValueChange) {
      useEffect(() => {
        onValueChange(value);
      }, [value]);
    }
    return (
      <ButtonGroup ref={ref} className="pra-form-selection-buttons" {...props}>
        {options.map(({ value: optionValue, label: optionLabel }) => {
          const pressed = optionValue === value;
          return (
            <ButtonBadge data-value={optionValue} pressed={pressed} onClick={_buttonClicked} appearance="primary">
              {optionLabel}
            </ButtonBadge>
          );
        })}
      </ButtonGroup>
    );
  },
);

FormSelectionButtons.displayName = 'FormSelectionButtons';
