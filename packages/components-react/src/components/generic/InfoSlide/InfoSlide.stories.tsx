import WelcomeVector1 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector1';
import WelcomeVector2 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector2';
import WelcomeVector3 from '@persoonlijke-regelingen-assistent/assets/dist/icons/custom/WelcomeVector3';
import user_login from '@persoonlijke-regelingen-assistent/assets/dist/images/user-login.png';
import user_todo from '@persoonlijke-regelingen-assistent/assets/dist/images/user-todo.png';
import user_welcome from '@persoonlijke-regelingen-assistent/assets/dist/images/user-welcome.png';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/InfoSlide/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { InfoSlide } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof InfoSlide> = {
  component: InfoSlide,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/InfoSlide',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'infoSlide',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof InfoSlide>;

export const Slide1: Story = {
  args: {
    backgroundVector: WelcomeVector1,
    imageSrc: user_welcome,
    imageAlt: 'user-welcome',
    information: 'Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae.',
  },
};

export const Slide2: Story = {
  args: {
    backgroundVector: WelcomeVector2,
    imageSrc: user_todo,
    imageAlt: 'user-todo',
    information: 'Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae.',
  },
};

export const Slide3: Story = {
  args: {
    backgroundVector: WelcomeVector3,
    imageSrc: user_login,
    imageAlt: 'user-login',
    information: 'Lorem ipsum dolor sit amet consectetur. Lacus a pellentesque sit nec id mauris imperdiet vitae.',
  },
};
