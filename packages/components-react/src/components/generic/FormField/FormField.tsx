import HelpIcon from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Help';
import { forwardRef, ForwardRefExoticComponent, PropsWithChildren, ReactNode, RefAttributes } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/FormField/FormField.scss';
import { FormLabel } from '../FormLabel';

interface FormFieldSpecificProps {
  type: 'textbox' | 'checkbox';
  label?: string;
  indicatorInfo?: string;
}

export interface FormFielddProps extends FormFieldSpecificProps {}

export const FormField: ForwardRefExoticComponent<
  FormFielddProps & { children?: ReactNode } & RefAttributes<HTMLDivElement>
> = forwardRef<HTMLDivElement, PropsWithChildren<FormFielddProps>>(({ children, label, type, indicatorInfo }, ref) => (
  <div ref={ref} className={`pra-form-field ${type ? `pra-form-field-${type}` : ''}`}>
    {label && <FormLabel className="pra-form-field-label">{label}</FormLabel>}
    {indicatorInfo && (
      <span title={indicatorInfo} className="pra-form-field-indicator">
        {HelpIcon({})}
      </span>
    )}
    <div className="pra-form-field-input">{children}</div>
  </div>
));

FormField.displayName = 'FormField';
