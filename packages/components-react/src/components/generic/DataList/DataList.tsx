import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { Heading } from '../Heading';

import '@persoonlijke-regelingen-assistent/components-css/DataList/DataList.scss';

interface DataListSpecificProps {
  headingLevel?: number;
  title?: string;
  items?: { key: string; value: string; multiline?: boolean }[];
}
export interface DataListProps extends DataListSpecificProps {}

export const DataList: React.ForwardRefExoticComponent<
  DataListProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ headingLevel, title, items }: PropsWithChildren<DataListProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref}>
        <Heading level={headingLevel || 3}>{title}</Heading>
        <dl className="pra-data-list pra-data-list--rows">
          {items?.map((item) => {
            return [
              <dt className="pra-data-list__item-key">{item.key}</dt>,
              <dd
                className={`pra-data-list__item-value pra-data-list__item-value--html-dd ${
                  item.multiline ? 'pra-data-list__item-value--multiline' : ''
                }`}
              >
                {item.value}
              </dd>,
            ];
          })}
        </dl>
      </div>
    );
  },
);

DataList.displayName = 'DataList';
