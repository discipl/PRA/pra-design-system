import ArrowRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ArrowRight';
import X from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/X';
import { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/NotificationMessage/NotificationMessage.scss';
import { Button } from '../Button';
import { Heading } from '../Heading';
import { Icon } from '../Icon';
import { Paragraph } from '../Paragraph';
interface NotificationMessageSpecificProps {
  notificationTitle: string;
  message: string;
  buttonLabel: string;
  buttonHandler: MouseEventHandler;
  closeHandler: MouseEventHandler;
}

export interface NotificationMessageProps
  extends NotificationMessageSpecificProps,
    HTMLAttributes<HTMLDivElement>,
    React.RefAttributes<HTMLDivElement> {}

export const NotificationMessage: React.ForwardRefExoticComponent<NotificationMessageProps> = forwardRef(
  (
    {
      notificationTitle,
      message,
      buttonLabel,
      buttonHandler,
      closeHandler,
      className,
      ...otherProps
    }: NotificationMessageProps,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className={`pra-notification-message ${className}`} {...otherProps}>
        <Heading className="pra-notification-message-title" level={4}>
          {notificationTitle}
        </Heading>
        <Button
          className="pra-notification-message-cross"
          appearance="subtle-button"
          onClick={(event) => closeHandler(event)}
        >
          <Icon>{X({ style: { inlineSize: '24px' } })}</Icon>
        </Button>
        <Paragraph className="pra-notification-message-content">{message}</Paragraph>
        {buttonHandler !== undefined && buttonLabel?.length > 0 && (
          <Button
            className="pra-notification-message-button"
            appearance="subtle-button"
            onClick={(event) => buttonHandler(event)}
          >
            {buttonLabel}
            <Icon>{ArrowRight({})}</Icon>
          </Button>
        )}
      </div>
    );
  },
);

NotificationMessage.displayName = 'NotificationMessage';
