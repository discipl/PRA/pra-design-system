import '@persoonlijke-regelingen-assistent/components-css/InformationAlert/InformationAlert.scss';
import X from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/X';
import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren, SVGProps } from 'react';
import { Heading } from '../Heading';
import { Icon } from '../Icon';

interface InformationAlertSpecificProps {
  // eslint-disable-next-line no-unused-vars
  icon?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
  title?: string;
}
export interface InformationAlertProps extends HTMLAttributes<HTMLInputElement>, InformationAlertSpecificProps {}

export const InformationAlert: React.ForwardRefExoticComponent<
  InformationAlertProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  ({ children, title, icon }: PropsWithChildren<InformationAlertProps>, ref: ForwardedRef<HTMLDivElement>) => {
    return (
      <div ref={ref} className="pra-information-alert">
        <Icon className="pra-information-alert-icon">{icon && icon({})}</Icon>
        <Heading level={3}>{title}</Heading>
        <span className="pra-information-alert-icon">{X({})}</span>
        <span className="pra-information-alert-content">{children}</span>
      </div>
    );
  },
);

InformationAlert.displayName = 'InformationAlert';
