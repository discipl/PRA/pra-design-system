import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import ReadMe from '@persoonlijke-regelingen-assistent/components-css/BreadcrumbNavSeparator/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { BreadcrumbNavSeparator } from './index';
import { BreadcrumbNav } from '../BreadcrumbNav';
import { Icon } from '../Icon';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
const meta: Meta<typeof BreadcrumbNavSeparator> = {
  component: BreadcrumbNavSeparator,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],
  title: 'PRA-DS/Components/BreadcrumbNavSeparator',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'breadcrumbnavseparator',
    ['--pra-component-design-version-name']: '0.0.0',
  },
  decorators: [
    (Story) => (
      <BreadcrumbNav>
        <Story />
      </BreadcrumbNav>
    ),
  ],
};

export default meta;
type Story = StoryObj<typeof BreadcrumbNavSeparator>;

export const Default: Story = {
  args: {
    children: [<Icon>{ChevronRight({})}</Icon>],
  },
};
