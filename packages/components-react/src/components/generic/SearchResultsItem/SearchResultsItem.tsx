import Bookmark from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Bookmark';
import React, { ForwardedRef, forwardRef, HTMLAttributes, PropsWithChildren } from 'react';
import { Heading } from '../Heading';
import { Paragraph } from '../Paragraph';

import '@persoonlijke-regelingen-assistent/components-css/SearchResultsItem/SearchResultsItem.scss';

interface SearchResultsItemSpecificProps {
  title?: string;
  subTitle?: string;
  description?: string;
}

export interface SearchResultsItemProps extends HTMLAttributes<HTMLLIElement>, SearchResultsItemSpecificProps {}

export const SearchResultsItem: React.ForwardRefExoticComponent<
  SearchResultsItemProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLLIElement>
> = forwardRef(
  (
    { title, subTitle, description, ...props }: PropsWithChildren<SearchResultsItemProps>,
    ref: ForwardedRef<HTMLLIElement>,
  ) => {
    return (
      <li ref={ref} className={`pra-search-results-item`} {...props}>
        <div className="pra-search-results-item-header">
          <Heading className="pra-search-results-item-title" level={3}>
            {title}
          </Heading>
          {Bookmark({})}
        </div>
        <Heading className="pra-search-results-item-subtitle" level={4}>
          {subTitle}
        </Heading>
        <Paragraph className="pra-search-results-item-description">{description}</Paragraph>
      </li>
    );
  },
);

SearchResultsItem.displayName = 'SearchResultsItem';
