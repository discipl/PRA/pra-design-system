import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ProgressTracker/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ProgressTracker } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof ProgressTracker> = {
  component: ProgressTracker,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/ProgressTracker',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'progresstracker',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ProgressTracker>;

export const EmptyProgressTracker: Story = {
  args: { progress: 0, total: 4 },
};

export const ProgressTrackerWithProgress: Story = {
  args: { progress: 2, total: 4 },
};
export const CompleteProgressTracker: Story = {
  args: { progress: 5, total: 4 },
};
