import '@persoonlijke-regelingen-assistent/components-css/NotificationItem/NotificationItem.scss';
import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';
import { Heading } from '../Heading';
import { Paragraph } from '../Paragraph';

interface NotificationItemSpecificProps {
  title?: string;
  message?: string;
  timeIndication?: string;
  newItem?: boolean;
}

export interface NotificationItemProps extends NotificationItemSpecificProps {}

export const NotificationItem: React.ForwardRefExoticComponent<
  NotificationItemProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLDivElement>
> = forwardRef(
  (
    { newItem, title, message, timeIndication }: PropsWithChildren<NotificationItemProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div ref={ref} className={`pra-notification-item ${newItem ? 'pra-notification-item-new' : ''}`}>
        <div className="pra-notification-item-title">
          <Heading level={3}>{title}</Heading>
          <span className="pra-notification-item-title-time-indication">{timeIndication}</span>
          {newItem ? <span className="pra-notification-item-title-new">NEW</span> : ''}
        </div>
        <Paragraph>{message}</Paragraph>
      </div>
    );
  },
);

NotificationItem.displayName = 'NotificationItem';
