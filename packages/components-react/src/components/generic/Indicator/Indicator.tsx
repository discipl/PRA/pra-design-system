import Bookmark from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Bookmark';
import BookmarkFilled from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/BookmarkFilled';
import Clock from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Clock';
import React, { ForwardedRef, forwardRef, HTMLAttributes, MouseEventHandler } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Indicator/Indicator.scss';

interface IndicatorSpecificProps {
  organisation?: string;
  indicationInMins: number;
  bookmarkOnClick: MouseEventHandler;
  isBookmarked: boolean;
}
export interface IndicatorProps extends HTMLAttributes<HTMLDivElement>, IndicatorSpecificProps {}

export const Indicator: React.ForwardRefExoticComponent<IndicatorProps & React.RefAttributes<HTMLDivElement>> =
  forwardRef(
    (
      { organisation, indicationInMins, bookmarkOnClick, isBookmarked, ...otherProps }: IndicatorProps,
      ref: ForwardedRef<HTMLDivElement>,
    ) => {
      const organisationLabel = organisation ? (
        <span key="organisationLabel" className="pra-indicator-organisation-label">
          {organisation}
        </span>
      ) : null;
      const indicatorLabel = (
        <span key="indicatorLabel" className="pra-indicator-label">{`${indicationInMins} minuten`}</span>
      );
      const bookmarkIcon = (
        <span key="bookmarkIcon" className="pra-indicator-bookmark-icon" onClick={(event) => bookmarkOnClick(event)}>
          {isBookmarked ? BookmarkFilled({}) : Bookmark({})}
        </span>
      );
      const clockIcon = (
        <span key="clockIcon" className="pra-indicator-clock-icon">
          {Clock({})}
        </span>
      );
      return (
        <div className="pra-indicator" ref={ref} {...otherProps}>
          {organisationLabel !== null
            ? [bookmarkIcon, clockIcon, indicatorLabel, organisationLabel]
            : [clockIcon, indicatorLabel, bookmarkIcon]}
        </div>
      );
    },
  );

Indicator.displayName = 'Indicator';
