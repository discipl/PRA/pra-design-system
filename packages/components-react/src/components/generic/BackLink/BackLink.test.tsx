import { render, screen } from '@testing-library/react';
import { createRef } from 'react';
import { BackLink } from './BackLink';
import '@testing-library/jest-dom';

describe('BackLink', () => {
  it('renders a link role element', () => {
    render(<BackLink>Terug</BackLink>);

    const backlink = screen.getByRole('link');

    expect(backlink).toBeInTheDocument();
    expect(backlink).toBeVisible();
  });

  it('renders an HTML backlink element', () => {
    const { container } = render(<BackLink />);

    const backlink = container.querySelector('a:only-child');

    expect(backlink).toBeInTheDocument();
  });

  it('renders labels that contain HTML rich text content', () => {
    const { container } = render(
      <BackLink>
        Order <strong>now</strong>
      </BackLink>,
    );

    const backlink = container.querySelector(':only-child');

    const richText = backlink?.querySelector('strong');

    expect(richText).toBeInTheDocument();
  });

  it('supports ForwardRef in React', () => {
    const ref = createRef<HTMLAnchorElement>();

    const { container } = render(<BackLink ref={ref} />);

    const backlink = container.querySelector(':only-child');

    expect(ref.current).toBe(backlink);
  });
});
