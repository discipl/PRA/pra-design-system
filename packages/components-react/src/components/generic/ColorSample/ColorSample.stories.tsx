import ReadMe from '@persoonlijke-regelingen-assistent/components-css/ColorSample/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { ColorSample } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof ColorSample> = {
  component: ColorSample,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/ColorSample',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'colorsample',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof ColorSample>;

export const Red: Story = {
  args: {
    color: 'red',
  },
};

export const Blue: Story = {
  args: {
    color: 'blue',
  },
};
