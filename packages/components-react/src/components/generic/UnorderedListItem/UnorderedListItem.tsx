import { ForwardedRef, forwardRef, PropsWithChildren, ReactNode } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/UnorderedListItem/UnorderedListItem.scss';

interface UnorderedListItemSpecificProps {
  markerContent?: string | ReactNode;
}

export interface UnorderedListItemProps extends UnorderedListItemSpecificProps {}

export const UnorderedListItem: React.ForwardRefExoticComponent<
  UnorderedListItemProps & {
    children?: ReactNode;
  } & React.RefAttributes<HTMLLIElement>
> = forwardRef(
  ({ markerContent = '-', children }: PropsWithChildren<UnorderedListItemProps>, ref: ForwardedRef<HTMLLIElement>) => {
    return (
      <li ref={ref} className="pra-unordered-list-item">
        {typeof markerContent === 'string' && <span className="pra-unordered-list-item-marker">{markerContent}</span>}
        {typeof markerContent === 'object' && <span className="pra-unordered-list-item-marker">{markerContent}</span>}
        {children}
      </li>
    );
  },
);

UnorderedListItem.displayName = 'UnorderedListItem';
