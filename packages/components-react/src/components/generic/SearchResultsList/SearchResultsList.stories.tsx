import ReadMe from '@persoonlijke-regelingen-assistent/components-css/SearchResultsList/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { SearchResultsList } from './index';
import { SearchResultsItem } from '../SearchResultsItem';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SearchResultsList> = {
  component: SearchResultsList,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SearchResultsList',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'searchresultslist',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof SearchResultsList>;

export const Default: Story = {
  args: {
    children: [
      <SearchResultsItem
        title="Titel Regeling"
        subTitle="Overheidsinstantie"
        description="01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt."
      />,
      <SearchResultsItem
        title="Titel Regeling"
        subTitle="Overheidsinstantie"
        description="01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt."
      />,
      <SearchResultsItem
        title="Titel Regeling"
        subTitle="Overheidsinstantie"
        description="01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt."
      />,
      <SearchResultsItem
        title="Titel Regeling"
        subTitle="Overheidsinstantie"
        description="01-01-2000 - Lorem ipsum dolor sit amet consectetur. Purus tellus at nunc amet quis ut augue tincidunt."
      />,
    ],
  },
};
