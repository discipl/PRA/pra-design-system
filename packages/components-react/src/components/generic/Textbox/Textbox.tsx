import { Textbox as UtrechtTextbox, TextboxProps as UtrechtTextboxProps } from '@utrecht/component-library-react';
import React, { ForwardedRef, forwardRef, InputHTMLAttributes } from 'react';
import '@persoonlijke-regelingen-assistent/components-css/Textbox/Textbox.scss';

interface TextboxSpecificProps {
  iconStart?: JSX.Element;
  iconEnd?: JSX.Element;
  invalid: UtrechtTextboxProps['invalid'];
}
export interface TextboxProps extends InputHTMLAttributes<HTMLInputElement>, TextboxSpecificProps {}

export const Textbox: React.ForwardRefExoticComponent<TextboxProps & React.RefAttributes<HTMLLabelElement>> =
  forwardRef(({ ...props }: TextboxProps, ref: ForwardedRef<HTMLLabelElement>) => {
    const { iconStart, iconEnd, placeholder, id, ...otherProps } = props;
    return (
      <label className="pra-textbox" ref={ref} htmlFor={id}>
        {iconStart && <span className="pra-icon pra-icon-start">{iconStart}</span>}
        <UtrechtTextbox {...otherProps} placeholder={placeholder} type="text" id={id} />
        {iconEnd && <span className="pra-icon pra-icon-end">{iconEnd}</span>}
      </label>
    );
  });

Textbox.displayName = 'Textbox';
