import ReadMe from '@persoonlijke-regelingen-assistent/components-css/Document/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { Document } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof Document> = {
  component: Document,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/Document',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'document',
    ['--pra-component-design-version-name']: '0.0.0',
  },
};

export default meta;
type Story = StoryObj<typeof Document>;

export const Default: Story = {
  args: {
    children: ['DocumentText'],
  },
};
