import ReadMe from '@persoonlijke-regelingen-assistent/components-css/SavedItem/README.md?raw';
import type { Meta, StoryObj } from '@storybook/react';
import { SavedItem } from './index';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const meta: Meta<typeof SavedItem> = {
  component: SavedItem,
  //👇 Enables auto-generated documentation for the component story
  tags: ['autodocs'],

  title: 'PRA-DS/Components/SavedItem',
  parameters: {
    docs: {
      description: {
        component: ReadMe,
      },
    },
    ['--pra-renderer-component-name']: 'notificationitem',
    ['--pra-component-design-version-name']: 'autosave - 4573412333',
    ['--pra-component-design-version-id']: '4573412333',
  },
};

export default meta;
type Story = StoryObj<typeof SavedItem>;

export const Default: Story = {
  args: {
    title: 'DigiD Aanvragen',
    message: 'Rijksoverheid',
  },
};
export const WithNotificationMessage: Story = {
  args: {
    ...Default.args,
    notificationMessage: 'Nog 3 dagen',
  },
};
