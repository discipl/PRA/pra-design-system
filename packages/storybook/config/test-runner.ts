import { TestRunnerConfig, waitForPageReady } from '@storybook/test-runner';
import { toMatchImageSnapshot } from 'jest-image-snapshot';

const customSnapshotsDir = `${process.cwd()}/__snapshots__`;

const config: TestRunnerConfig = {
  setup() {
    expect.extend({ toMatchImageSnapshot });
  },
  async postVisit(page, context) {
    // Waits for the page to be ready before taking a screenshot to ensure consistent results

    await page.setViewportSize({ width: 393, height: 800 });

    await waitForPageReady(page);
    // Workaround to ensure that images are fully imported and rendered
    await page.waitForLoadState('networkidle', { timeout: 300 });
    // To capture a screenshot for for different browsers, add page.context().browser().browserType().name() to get the browser name to prefix the file name
    const image = await page.screenshot();

    expect(image).toMatchImageSnapshot({
      customSnapshotsDir,
      customSnapshotIdentifier: `${context.id} - ${page?.context()?.browser()?.browserType()?.name()}`,
    });
  },
};
export default config;
